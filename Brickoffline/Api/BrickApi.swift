//
//  BrickApi.swift
//  Brickoffline
//
//  Created by Filip Jabłoński on 09/02/2022.
//

import Foundation

final class BrickApi {
    public static let instance = BrickApi(brickApiHost: Rebricable())
    private let connector: Connector
    private init (brickApiHost: BrickApiHost) {
        connector = BrickConnector(brickApiHost: brickApiHost)
    }
    
    func call<T:BrickMethod>(method: T) async throws -> T.Response {
        return try await connector.connect(method: method)
    }
}
