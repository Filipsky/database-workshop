//
//  BrickMethod.swift
//  Brickoffline
//
//  Created by Filip Jabłoński on 08/02/2022.
//

import Foundation

enum HttpMethod: String {
    case POST
    case GET
}

protocol BrickMethod {
    associatedtype Response: BrickResponse
    var path: String { get }
    var queryItems: [URLQueryItem] { get }
    var httpMethod: HttpMethod { get }
    
}

protocol GetBrickMethod: BrickMethod {}
extension GetBrickMethod {
    var httpMethod: HttpMethod { .GET }
}


protocol PostBrickMethod: BrickMethod, Encodable {
    func basicEncode(to encoder: Encoder) throws
}

extension PostBrickMethod {
    var httpMethod: HttpMethod  { .POST }
}

protocol BrickResponse: Decodable {}


public enum BrickError: LocalizedError {
    case invalidRequest
    
    public var errorDescription: String? {
        switch self {
        case .invalidRequest:
            return "Request url is not valid"
        }
    }
}
