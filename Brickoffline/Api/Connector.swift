//
//  Connector.swift
//  Brickoffline
//
//  Created by Filip Jabłoński on 01/02/2022.
//

import Foundation

protocol BrickApiHost {
    var authorization: URLQueryItem { get }
    var host: String { get }
    var decodingStrategy: JSONDecoder.KeyDecodingStrategy { get }
}

protocol Connector {
    func connect<T:BrickMethod>(method: T) async throws -> T.Response
}

struct BrickConnector: Connector {
    let session: URLSession
    let brickApiHost: BrickApiHost
    init(brickApiHost: BrickApiHost) {
        session = URLSession.shared
        self.brickApiHost = brickApiHost
    }
    func connect<T:BrickMethod>(method: T) async throws -> T.Response {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = brickApiHost.decodingStrategy
        var urlComponent = URLComponents()
        urlComponent.host = brickApiHost.host
        urlComponent.scheme = "https"
        urlComponent.path = method.path
        var queryItems:[URLQueryItem] = []
        queryItems.append(contentsOf: method.queryItems)
        queryItems.append(brickApiHost.authorization)
        urlComponent.queryItems = queryItems
        
        guard let url = urlComponent.url else {
            throw BrickError.invalidRequest
        }
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.httpMethod.rawValue
        
        let (data, _) = try await session.data(for: urlRequest)
        return try decoder.decode(T.Response.self, from: data)
    }
}
