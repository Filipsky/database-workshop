//
//  FindMiniFigures.swift
//  Brickoffline
//
//  Created by Filip Jabłoński on 08/02/2022.
//

import Foundation

struct FindMiniFigures: GetBrickMethod {
    typealias Response = FindMiniFiguresResponse
    let path: String = "/api/v3/lego/minifigs"
    let queryItems: [URLQueryItem]
    let httpMethod: HttpMethod = .GET
    
    init(setId: String) {
        self.queryItems = [URLQueryItem(name: "in_set_num", value: setId.appending("-1"))]
    }
    
}

struct FindMiniFiguresResponse: BrickResponse {
    let count: Int
    let next: URL?
    let previous: URL?
    let results: [MiniFigure]
}
