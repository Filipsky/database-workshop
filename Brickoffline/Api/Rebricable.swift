//
//  Rebricable.swift
//  Brickoffline
//
//  Created by Filip Jabłoński on 15/01/2022.
//

import Foundation

struct Rebricable: BrickApiHost {
    private let apiKey = "PASTE API KEY HERE"
    public let host = "rebrickable.com"
    
    var authorization: URLQueryItem {
        URLQueryItem(name: "key", value: apiKey)
    }
    
    let decodingStrategy: JSONDecoder.KeyDecodingStrategy = .convertFromSnakeCase
}
