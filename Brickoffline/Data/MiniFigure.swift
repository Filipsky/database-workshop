//
//  Minifigure.swift
//  Brickoffline
//
//  Created by Filip Jabłoński on 13/02/2022.
//

import Foundation

struct MiniFigure: Decodable, Hashable {
    let setNum: String
    let name: String
    let numParts: Int
    let setImgUrl: String
    let setUrl: String
    let lastModifiedDt: String
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(setNum)
        hasher.combine(name)
    }
}
