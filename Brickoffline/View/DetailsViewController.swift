//
//  DetailsViewController.swift
//  Brickoffline
//
//  Created by Filip Jabłoński on 12/02/2022.
//

import UIKit
import RealmSwift
import Kingfisher

final class DetailsViewController: UIViewController {
    
    @IBOutlet var setImage: UIImageView?
    @IBOutlet var titleLabel: UILabel?
    @IBOutlet var setNumberLabel: UILabel?
    @IBOutlet var lastModified: UILabel?
    @IBOutlet var saveButton: UIButton?
    var minifigure: MiniFigure?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateDetails(minifigure: minifigure!)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func updateDetails(minifigure: MiniFigure) {
        setImage?.kf.setImage(with: URL(string: String(minifigure.setImgUrl))!, options: [.transition(.fade(1)), .onlyFromCache])
        titleLabel?.text = minifigure.name
        setNumberLabel?.text = minifigure.setNum
        lastModified?.text = minifigure.lastModifiedDt
    }
    
    @IBAction func save(sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }

}
