//
//  ViewController.swift
//  Brickoffline
//
//  Created by Filip Jabłoński on 10/01/2022.
//

import UIKit
import RealmSwift
import Kingfisher

final class MiniFiguresTableViewController: UITableViewController {
    
    private let cellIdentifier: String = "cell"

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpTableView()
    }
    
    private func setUpTableView() {
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellIdentifier)
    }
}

