//
//  ResultsViewController.swift
//  Brickoffline
//
//  Created by Filip Jabłoński on 15/01/2022.
//

import UIKit
import Kingfisher

final class ResultsTableViewController: UITableViewController {
    
    var minifigures:[MiniFigure] = []
    private let reusableIdentifier = "MinifigureCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: reusableIdentifier)
        tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return minifigures.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reusableIdentifier, for: indexPath)
        let data = minifigures[indexPath.row]
        cell.textLabel?.text = data.name
        cell.imageView?.contentMode = .scaleAspectFit
        cell.imageView?.kf.setImage(with: URL(string: String(data.setImgUrl))!, options: [.transition(.fade(1)), .cacheMemoryOnly]) { [weak self] r in
            switch r {
            case .success:
                self?.updateLayout()
            case .failure:
                break
            }
        }
        return  cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let viewController = storyboard?.instantiateViewController(withIdentifier: "DetailsViewController") as! DetailsViewController
        viewController.minifigure = minifigures[indexPath.row]
        
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    private func updateLayout() {
        tableView.beginUpdates()
        tableView.endUpdates()
    }
}

