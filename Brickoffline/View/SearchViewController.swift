//
//  SearchViewController.swift
//  Brickoffline
//
//  Created by Filip Jabłoński on 15/01/2022.
//

import Foundation
import UIKit

final class SearchViewController: UIViewController {
    @IBOutlet var setNumber: UITextField?
    
    @IBAction func search(sender: UIButton) {
        Task {
            do {
                guard let setNumberId = setNumber?.text else { return }
                let results = try await BrickApi.instance.call(method: FindMiniFigures(setId: setNumberId))
                let resultsViewController = storyboard?.instantiateViewController(withIdentifier: "ResultsTableViewController") as! ResultsTableViewController
                resultsViewController.minifigures = results.results
                navigationController?.pushViewController(resultsViewController, animated: true)
            } catch {
                print(String(describing: error))
            }
        }
    }
}
