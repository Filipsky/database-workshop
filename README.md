## Tooling requirements
- Xcode 13.2.1+ (https://apps.apple.com/pl/app/xcode/id497799835?l=pl&mt=12)

## Before you start
- Before you start you need to get your own API key from https://rebrickable.com/.
- Create an account (it is free)
- Generate API key like here [video](video/generate-api-key.mov)
- Paste API key in [Rebricable.swft](Brickoffline/Api/Rebricable.swift) under `apiKey` const